package com.post;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"com.post.service"})
@ComponentScan(basePackages = {"com.post.service.impl"})
@ComponentScan(basePackages = {"com.post.controller"})
@ComponentScan(basePackages = {"com.post.model"})
@ComponentScan(basePackages = {"com.post.repository"})

@SpringBootApplication
@EnableDiscoveryClient
public class PostApplication {

	public static void main(String[] args) {
		SpringApplication.run(PostApplication.class, args);
	}
}
