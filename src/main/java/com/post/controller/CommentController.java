package com.post.controller;

import com.post.model.dto.comment.CommentDto;
import com.post.model.dto.response.ResponseGeneralDto;
import com.post.model.entity.Comment;
import com.post.service.CommentService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/comment")
@AllArgsConstructor
public class CommentController {

    private final CommentService commentService;

    /**
     * Add a comment to a post
     */
    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    ResponseGeneralDto<Comment> addComment(@RequestBody CommentDto commentDto) {
        return commentService.addComment(commentDto);
    }
}
