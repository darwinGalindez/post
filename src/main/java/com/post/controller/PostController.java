package com.post.controller;

import com.post.model.dto.post.PostDto;
import com.post.model.dto.response.ResponseGeneralDto;
import com.post.model.entity.Post;
import com.post.service.PostService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/***
 *
 *
 *  MATCH CONTROLLER
 *
 *
 * ***/
@RestController
@RequestMapping("/post")
@AllArgsConstructor
public class PostController {

	private final PostService postService;

	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseGeneralDto<Post> createPost(@RequestBody PostDto postDto, HttpServletRequest request) {
		var responseService = postService.createPost(postDto);
		setRequestUri(request, responseService);

		return responseService;
	}

	@PatchMapping(path = "/visit/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseGeneralDto<Post> visit(@PathVariable(value = "id") int id, HttpServletRequest request) {
		var responseService = postService.visit(id);
		setRequestUri(request, responseService);

		return responseService;
	}

	@GetMapping(path = "/by-user/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseGeneralDto<Post> getAllByUser(@PathVariable(value = "userId") int userId, @RequestParam int page, @RequestParam int size,
										  HttpServletRequest request) {
		var responseService = postService.getAllByUser(userId, page, size);
		setRequestUri(request, responseService);

		return responseService;
	}

	private void setRequestUri(HttpServletRequest request, ResponseGeneralDto<Post> responseGeneralDto) {
		responseGeneralDto.setUrl(request.getRequestURI());
	}
}
