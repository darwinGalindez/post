package com.post.exception;

import com.post.model.dto.response.ResponseGeneralDto;
import com.post.model.entity.Post;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerExceptions {

    private static final int CODE_UNHANDLED_ERROR = 0;
    private static final String MESSAGE_UNHANDLED_ERROR = "Service failed. Please try again later";

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(value = Throwable.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseGeneralDto<Post> processUnhandledError(Exception e) {

        logger.error(e.toString());
        return new ResponseGeneralDto<>(CODE_UNHANDLED_ERROR, MESSAGE_UNHANDLED_ERROR);
    }
}
