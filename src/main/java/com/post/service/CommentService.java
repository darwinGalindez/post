package com.post.service;


import com.post.model.dto.comment.CommentDto;
import com.post.model.dto.response.ResponseGeneralDto;
import com.post.model.entity.Comment;
import org.springframework.stereotype.Service;

@Service
public interface CommentService {

    /**
     * Add a comment to a post
     * @param commentDto
     *
     * return ResponseGeneralDto<Post>
     */
    ResponseGeneralDto<Comment> addComment(CommentDto commentDto);

}
