package com.post.service;

import org.springframework.stereotype.Service;

@Service
public interface ModelMapperService<T, E> {

    /**
     * Convert DTO to ENTITY
     */
    E convertDtoToEntity(T dto, Class<E> entityClass);
}
