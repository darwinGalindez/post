package com.post.service;


import com.post.model.dto.post.PostDto;
import com.post.model.dto.response.ResponseGeneralDto;
import com.post.model.entity.Post;
import org.springframework.stereotype.Service;

import java.util.Collection;

/***
 *
 *  TODO:
 *
 * **/
@Service
public interface PostService {

    /**
     *
     * Create a post with a given user id in the request
     *
     * **/
    ResponseGeneralDto<Post> createPost(PostDto postDto);

    /***
     *
     * Get a post's information and register a visit to it
     *
     * **/
    ResponseGeneralDto<Post> visit(int postId);

    /**
     *
     * Get all posts made by a given user (paginated)
     *
     * **/
    ResponseGeneralDto<Post> getAllByUser(int userId, int page, int size);
}
