package com.post.service.impl;

import com.post.model.dto.comment.CommentDto;
import com.post.model.dto.response.ResponseGeneralDto;
import com.post.model.entity.Comment;
import com.post.repository.CommentRepository;
import com.post.service.CommentService;
import com.post.service.ModelMapperService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CommentServiceImpl implements CommentService {

    private static final int CODE_SUCCESS = 1;
    private static final String MESSAGE_CREATE_SUCCESS = "The comment was created correctly.";

    private CommentRepository commentRepository;
    private ModelMapperService<CommentDto, Comment> modelMapperService;

    @Override
    public ResponseGeneralDto<Comment> addComment(CommentDto commentDto) {

        var comment = modelMapperService.convertDtoToEntity(commentDto, Comment.class);
        commentRepository.save(comment);

        return new ResponseGeneralDto<>(CODE_SUCCESS, MESSAGE_CREATE_SUCCESS);
    }
}
