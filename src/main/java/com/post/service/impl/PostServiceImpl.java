package com.post.service.impl;

import com.post.exception.PostException;
import com.post.model.dto.PageableDto;
import com.post.model.dto.post.PostDto;
import com.post.model.dto.response.ResponseGeneralDto;
import com.post.model.entity.Post;
import com.post.repository.PostRepository;
import com.post.service.ModelMapperService;
import com.post.service.PostService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class PostServiceImpl implements PostService {

    private static final int CODE_SUCCESS = 1;
    private static final int CODE_ERROR = 0;
    private static final String MESSAGE_CREATE_SUCCESS = "The post was created correctly.";
    private static final String MESSAGE_NOT_EXIST_POST = "The post selected doesn't exist";
    private static final String MESSAGE_SUCCESS_OPERATION = "Success operation";

    private PostRepository postRepository;

    private ModelMapperService<PostDto, Post> modelMapperService;

    @Override
    public ResponseGeneralDto<Post> createPost(PostDto postDto) {

        var post = modelMapperService.convertDtoToEntity(postDto, Post.class);
        postRepository.save(post);

        return new ResponseGeneralDto<>(CODE_SUCCESS, MESSAGE_CREATE_SUCCESS);
    }

    @Override
    public ResponseGeneralDto<Post> visit(int postId) {
        var post = getPostById(postId);
        post.setViews(post.getViews() + 1);
        postRepository.save(post);

        return new ResponseGeneralDto<>(CODE_SUCCESS, MESSAGE_SUCCESS_OPERATION, List.of(post));
    }

    @Override
    public ResponseGeneralDto<Post> getAllByUser(int userId, int page, int size) {

        var pageRequest = PageRequest.of(page, size);
        var postsPaginated = postRepository.findAllByUserId(userId, pageRequest);
        var pageable = new PageableDto(postsPaginated.getPageable(), postsPaginated.getTotalPages(), ((int) postsPaginated.getTotalElements()));

        return new ResponseGeneralDto<>(CODE_SUCCESS, MESSAGE_SUCCESS_OPERATION, postsPaginated.toList(), pageable);
    }

    private Post getPostById(int postId) {
        var post = postRepository.findById(postId);

        if (post.isEmpty()) {
            throw new PostException(CODE_ERROR, MESSAGE_NOT_EXIST_POST);
        }

        return post.get();
    }
}
