package com.post.service.impl;

import com.post.service.ModelMapperService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ModelMapperImpl implements ModelMapperService {

    @Override
    public Object convertDtoToEntity(Object dto, Class entityClass) {
        var modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.STRICT);

        return modelMapper.map(dto, entityClass);
    }
}
