package com.post.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

/**
 * TODO:
 * - Add the proper Spring annotations to the entity and its fields
 * - Include relations between entities
 */
@Getter
@Setter
@Entity(name = "comments")
@AllArgsConstructor
@NoArgsConstructor
public class Comment extends Auditable<String> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "comments_id_seq")
	@SequenceGenerator(name = "comments_seq", sequenceName = "comments_id_seq")
	@Basic(optional = false)
	private Integer id;

	private String body;

	@Column(name = "post_id", columnDefinition = "INTEGER, FOREIGN KEY (post_id) REFERENCES posts(id)")
	private Integer postId;

	@Column(name = "author_id", columnDefinition = "INTEGER, FOREIGN KEY (author_id) REFERENCES users(id)")
	private Integer authorId;
}
