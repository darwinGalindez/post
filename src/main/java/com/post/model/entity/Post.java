package com.post.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import java.util.List;

/**
 * TODO:
 * - Add the proper Spring annotations to the entity and its fields
 * - Include relations between entities
 */
@Getter
@Setter
@Entity(name = "posts")
@AllArgsConstructor
@NoArgsConstructor
public class Post extends Auditable<String> {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "posts_id_seq")
	@SequenceGenerator(name = "posts_seq", sequenceName = "posts_id_seq")
	@Basic(optional = false)
	private Integer id;

	private Integer views;
	private String title;
	private String body;

	@Column(name = "user_id", columnDefinition = "INTEGER, FOREIGN KEY (user_id) REFERENCES users(id)")
	private Integer userId;
}
