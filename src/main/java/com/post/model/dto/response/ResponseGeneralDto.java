package com.post.model.dto.response;

import com.post.model.dto.PageableDto;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ResponseGeneralDto<T> {

    private int code;
    private String message;
    private String url;
    private List<T> parameters;
    private PageableDto pageData;

    public ResponseGeneralDto(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public ResponseGeneralDto(int code, String message, List<T> parameters) {
        this.code = code;
        this.message = message;
        this.parameters = parameters;
    }

    public ResponseGeneralDto(int code, String message, List<T> parameters, PageableDto pageData) {
        this.code = code;
        this.message = message;
        this.parameters = parameters;
        this.pageData = pageData;
    }
}
