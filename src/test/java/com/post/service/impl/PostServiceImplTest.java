package com.post.service.impl;

import com.post.model.dto.post.PostDto;
import com.post.model.dto.post.testdatabuilder.PostDtoTestDataBuilder;
import com.post.model.entity.Post;
import com.post.model.entity.testdatabuilder.PostTestDataBuilder;
import com.post.repository.PostRepository;
import com.post.service.ModelMapperService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.List;
import java.util.Optional;

public class PostServiceImplTest {

    private static final int CODE_SUCCESS = 1;
    private static final int QUANTITY_CALLS = 1;
    private static final int PAGE = 1;
    private static final int SIZE = 10;

    private PostRepository postRepository;

    private ModelMapperService<PostDto, Post> modelMapperService;
    private PostServiceImpl postServiceImpl;

    @BeforeEach
    void setUp() {
        postRepository = Mockito.mock(PostRepository.class);
        modelMapperService = Mockito.mock(ModelMapperService.class);
        postServiceImpl = new PostServiceImpl(postRepository, modelMapperService);
    }

    @Test
    void createPostTest() {
        var post = new PostTestDataBuilder().build();
        var postDto = new PostDtoTestDataBuilder().build();

        Mockito.when(modelMapperService.convertDtoToEntity(Mockito.any(), Mockito.any()))
                .thenReturn(post);
        Mockito.when(postRepository.save(post))
                .thenReturn(post);

        var responseService = postServiceImpl.createPost(postDto);

        Mockito.verify(modelMapperService, Mockito.times(QUANTITY_CALLS)).convertDtoToEntity(Mockito.any(), Mockito.any());
        Mockito.verify(postRepository, Mockito.times(QUANTITY_CALLS)).save(post);
        Assertions.assertEquals(CODE_SUCCESS, responseService.getCode());
    }

    @Test
    void visitTest() {
        var post = new PostTestDataBuilder().build();

        Mockito.when(postRepository.findById(post.getId()))
                .thenReturn(Optional.of(post));
        Mockito.when(postRepository.save(post))
                .thenReturn(post);

        var responseService = postServiceImpl.visit(post.getId());

        Mockito.verify(postRepository, Mockito.times(QUANTITY_CALLS)).findById(post.getId());
        Mockito.verify(postRepository, Mockito.times(QUANTITY_CALLS)).save(post);
        Assertions.assertEquals(CODE_SUCCESS, responseService.getCode());
    }

    @Test
    void getAllByUserTest() {
        var post = new PostTestDataBuilder().build();
        var pageRequest = PageRequest.of(PAGE, SIZE);
        var posts = List.of(post);
        var postsPaginated = new PageImpl<>(posts);

        Mockito.when(postRepository.findAllByUserId(post.getUserId(), pageRequest))
                .thenReturn(postsPaginated);

        var responseService = postServiceImpl.getAllByUser(post.getUserId(), PAGE, SIZE);

        Mockito.verify(postRepository, Mockito.times(QUANTITY_CALLS)).findAllByUserId(post.getUserId(), pageRequest);
        Assertions.assertEquals(PAGE, responseService.getParameters().size());
        Assertions.assertEquals(CODE_SUCCESS, responseService.getCode());
    }
}
