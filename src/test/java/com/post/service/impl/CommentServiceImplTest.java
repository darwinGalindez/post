package com.post.service.impl;

import com.post.model.dto.comment.CommentDto;
import com.post.model.dto.comment.testdatabuilder.CommentDtoTestDataBuilder;
import com.post.model.entity.Comment;
import com.post.model.entity.testdatabuilder.CommentTestDataBuilder;
import com.post.repository.CommentRepository;
import com.post.service.ModelMapperService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class CommentServiceImplTest {

    private static final int CODE_SUCCESS = 1;
    private static final int QUANTITY_CALLS = 1;

    private CommentRepository commentRepository;
    private ModelMapperService<CommentDto, Comment> modelMapperService;
    private CommentServiceImpl commentServiceImpl;

    @BeforeEach
    void setUp() {
        commentRepository = Mockito.mock(CommentRepository.class);
        modelMapperService = Mockito.mock(ModelMapperService.class);
        commentServiceImpl = new CommentServiceImpl(commentRepository, modelMapperService);
    }

    @Test
    void addCommentTest() {
        var comment = new CommentTestDataBuilder().build();
        var commentDto = new CommentDtoTestDataBuilder().build();

        Mockito.when(modelMapperService.convertDtoToEntity(Mockito.any(), Mockito.any()))
                .thenReturn(comment);
        Mockito.when(commentRepository.save(comment))
                .thenReturn(null);

        var responseService = commentServiceImpl.addComment(commentDto);

        Mockito.verify(modelMapperService, Mockito.times(QUANTITY_CALLS)).convertDtoToEntity(Mockito.any(), Mockito.any());
        Mockito.verify(commentRepository, Mockito.times(QUANTITY_CALLS)).save(Mockito.any());
        Assertions.assertEquals(CODE_SUCCESS, responseService.getCode());
    }
}
