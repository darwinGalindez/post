package com.post.model.entity.testdatabuilder;

import com.post.model.entity.Post;

public class PostTestDataBuilder {

    private Integer id;
    private Integer views;
    private String title;
    private String body;
    private Integer userId;

    public PostTestDataBuilder() {
        id = 1;
        views = 0;
        title = "Test";
        body = "Test";
        userId = 2;
    }

    public Post build() {
        return new Post(id, views, title, body, userId);
    }
}
