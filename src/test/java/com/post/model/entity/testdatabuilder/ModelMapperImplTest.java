package com.post.model.entity.testdatabuilder;

import com.post.model.dto.post.testdatabuilder.PostDtoTestDataBuilder;
import com.post.model.entity.Post;
import com.post.service.impl.ModelMapperImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ModelMapperImplTest {

    private ModelMapperImpl modelMapperImpl;

    @BeforeEach
    void setUp() {
        modelMapperImpl = new ModelMapperImpl();
    }

    @Test
    void convertDtoToEntityTest() {

        var postDto = new PostDtoTestDataBuilder().build();
        var post = modelMapperImpl.convertDtoToEntity(postDto, Post.class);

        Assertions.assertEquals("Post", post.getClass().getSimpleName());
    }
}
