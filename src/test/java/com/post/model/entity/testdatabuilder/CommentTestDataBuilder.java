package com.post.model.entity.testdatabuilder;

import com.post.model.entity.Comment;

public class CommentTestDataBuilder {

    private Integer id;
    private String body;
    private Integer postId;
    private Integer authorId;

    public CommentTestDataBuilder() {
        id = 1;
        body = "Test";
        postId = 2;
        authorId = 2;
    }

    public Comment build() {
        return new Comment(id, body, postId, authorId);
    }
}
