package com.post.model.dto.comment.testdatabuilder;

import com.post.model.dto.comment.CommentDto;

public class CommentDtoTestDataBuilder {

    private String body;
    private Integer postId;
    private Integer authorId;

    public CommentDtoTestDataBuilder() {
        body = "Test";
        postId = 2;
        authorId = 2;
    }

    public CommentDto build() {
        return new CommentDto(body, postId, authorId);
    }
}
