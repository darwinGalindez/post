package com.post.model.dto.post.testdatabuilder;

import com.post.model.dto.post.PostDto;

public class PostDtoTestDataBuilder {

    private Integer views;
    private String title;
    private String body;
    private Integer userId;

    public PostDtoTestDataBuilder() {
        views = 1;
        title = "Test";
        body = "Test";
        userId = 2;
    }

    public PostDto build() {
        return new PostDto(views, title, body, userId);
    }
}
